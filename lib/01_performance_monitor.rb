require "time"

def measure(default=1, &prc)
  total_time = 0
  default.times do
    t1 = Time.now
    prc.call
    t2 = Time.now
    total_time += (t2 - t1)
  end
  total_time / default.to_f
end
