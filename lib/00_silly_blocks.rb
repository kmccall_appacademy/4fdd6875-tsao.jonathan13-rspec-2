def reverser(&prc)
  result = ""
  str = prc.call.split(" ")
  return prc.call.reverse if str.length == 1
  str.each do |word|
    result << " " + word.reverse
  end
  result[1...result.length]
end

def adder(default=1, &prc)
  prc.call + default
end

def repeater(default=1, &prc)
  default.times {prc.call}
end
